let tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

console.log('1) Общее количество времени , затраченное на работу над задачами из категории \'Frontend\': ')
console.log(
    tasks.reduce((acc, item) => {
        if (item.category === 'Frontend') {
            return item.timeSpent + acc
        }
        return acc
    }, 0)
);



console.log('2) Общее количество времени, затраченное на работу над задачами типа \'bug\': ')
console.log(
    tasks.reduce((acc, item) => {
        if (item.type === 'bug') {
            return item.timeSpent + acc
        }
            return acc
    }, 0)
);


const quantityCategory = tasks.reduce((acc, item) => {
    const category = item.category;

    if (category in acc) {
        acc[category]++;
    } else {
        acc[category] = 1;
    }
    return acc;
}, {});

console.log('3) Количество задач каждой категории в объекте: ')
console.log(quantityCategory);


const UITask = tasks.filter(task => task.title.includes('UI')).length;
console.log('4) Количество задач, имеющих в названии слово "UI": ');
console.log(UITask);


console.log('5) Массив задач с затраченным временем больше 4 часов: ')
console.log(
    tasks.reduce((acc, item) => {
        if (item.timeSpent > 4) {
            acc.push('Title: ' + item.title + ', ' + 'Category: ' + item.category)
        }
        return acc
    }, [])
);



